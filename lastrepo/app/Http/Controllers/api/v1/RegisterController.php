<?php


namespace App\Http\Controllers\api\v1;

use App\Http\Resources\User;
use App\Models\Customer;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator as Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Exceptions\HttpResponseException;


class RegisterController extends Controller
{
    /**
     * Handles Registration Request
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'username' => 'required|max:255|unique:customers',
            'country' => 'required|max:255',
            'email' => 'required|email|max:255|unique:customers',
            'phone' => 'required|unique:customers|min:10',
            'password' => 'required|min:6',
        ]);
        if ($validator->fails())
        {
            return $this->failedValidation($validator);
            return null;
        }

        $user = Customer::create([
            'first_name' => $request['first_name'],
            'last_name' => $request['last_name'],
            'username' => $request['username'],
            'email' => $request['email'],
            'phone' => $request['phone'],
            'country' => $request['country'],
            'password' => bcrypt($request['password']),
        ]);

        return response()->json(['token' => $user], 200);
    }
    protected function failedValidation( $validator) {
        $errors =  $validator->errors()->getMessages();
        $temp = array();
        foreach ($errors as $key => $message)
            $temp[$key] = $message[0];
        $userResouce = new User($temp);
        User::$wrap = 'error';
        return $userResouce;
        throw new HttpResponseException($userResouce);
    }
    /**
     * Failed validation disable redirect
     *
     * @param Validator $validator
     */

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
